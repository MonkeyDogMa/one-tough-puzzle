import itertools as it
import time
import sys

debug=0

def pause():
      programPause = input("Press the <ENTER> key to continue...")

perms = it.permutations([0,1,2,3,4,5,6,7,8])


#
# constants for readablity of the conversions
#
h=1 #heart tab
H=-1 #heart blank
d=2 # diamond tab
D=-2 # diamond blank
c=3  # club tab
C=-3 # club blank
s=4  # spade tab
S=-4 # spade blank


#
# this was the solution to my one tough puzzle
#
block = [
            [-4, -1, 3, 1],#SHch
            [-3, -2, 2, 3],#CDdc
            [-1, -3, 4, 4],#HCss
            [-2, -1, 1, 2],#DHhd
            [-1, 4, 2, -4],#HsdS
            [-3, 1, 4, -4],#ChsS
            [-2, 4, 2, -1],#DsdH
            [-2, -3, 3, 1],#DCch 
            [-3, -3, 1, 2] #CChd
        ]


#
# list of pieces.  uppercase letters are used to identify tab parts, lowercase letters are used to identify the blank parts
#
data = [ 
          [S,H,c,h],
          [C,D,d,c],
          [H,C,s,s],
          [D,C,c,h],
          [D,H,h,d],
          [S,C,h,s],
          [S,H,s,d],
          [C,C,h,d],
          [H,D,s,d]
       ]


#
#  checks are the rules for each puzzle position piece
#  puzzle placement order:  middle, bottom middle, bottom left, left middle, left top, top middle, right top, right middle, right bottom 
#
def checks(): 
    return abs(block[0][2] + block[1][0])

def checks1(): 
    if ( abs(block[1][0] + block[0][2]) == 0 and abs(block[1][3] + block[2][1]) == 0 ):
        return 0
    else:
        return 1

def checks2(): 
    if ( abs(block[2][0] + block[3][2]) == 0 and abs(block[2][1] + block[1][3]) == 0 ):
        return 0
    else:
        return 1

def checks3():
    if ( abs(block[3][1] + block[0][3]) == 0 and abs(block[3][0] + block[4][2]) == 0 and abs(block[3][2] + block[2][0]) == 0 ):
        return 0
    else:
        return 1

def checks4(): 
    if ( abs(block[4][1] + block[5][3]) == 0 and abs(block[4][2] + block[3][0]) == 0 ):
        return 0
    else:
        return 1
 
def checks5():
    if ( abs(block[5][2] + block[0][0]) == 0 and abs(block[5][1] + block[6][3]) == 0 and abs(block[5][3] + block[4][1] == 0) ):
        return 0
    else:
        return 1
    

def checks6(): 
    if ( abs(block[6][2] + block[7][0]) == 0 and abs(block[6][3] + block[5][1]) == 0 ):
        return 0
    else:
        return 1

def checks7():
    if ( abs(block[7][0] + block[6][2]) == 0 and abs(block[7][2] + block[8][0]) == 0 and abs(block[7][3] + block[0][1]) == 0 ):
        return 0
    else:
        return 1
    

def checks8():  
    if ( abs(block[8][0] + block[7][2]) == 0 and abs(block[8][3] + block[1][1]) == 0 ):
        print("Done:^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ")
        print(block)
        sys.exit()
        return 0
    else:
        #print(block)
        return 1
    

checks = [checks,checks1,checks2,checks3,checks4,checks5,checks6,checks7,checks8]


# 
# rotate will rotate the piece 
#
def rotate(num):
    if debug: print("rotate num: ",num)
    if debug: print("rotate: ",block[num])
    part1 = block[num][1:4]
    part1.append(block[num][0])
    block[num] = part1



# 
# block_check does the recursion 
#
def block_check(num):
    for x in range(4):
      #print("block_check num ", num)
      #pause()
      if( checks[num]() == 0 ):
         if debug: print("#######################################block fits############")
         block_check(num + 1) 
      else:
         if debug: print("rotate block*************************************************")
         if ( num + 1 == 9 ):
             return
         rotate( num + 1 )
     

# 
# arrange_block_array is needed to put the puzzle pieces in the correct order before verification
#
def arrange_block_array(pseq):
    global block 
    temp = []
    for dataBlock in pseq:
        temp.append( data[dataBlock] )
    block = temp


# 
# main_run uses brute force to try all permutations of the puzzle
#
def main_run():
  count=0
  for pseq in perms:
     print("start program", pseq )
     count += 1
     print(count)
     arrange_block_array(pseq)
     for element in range(3):
         global block
         block_check(1)
         first_element = block[0][1:9]
         first_element.append(block[0][0])
         block[0] = first_element


# 
# single_run is used to test a list of answers
#
def single_run():
  for element in range(3):
    global block
    block_check(0)
    if debug: print('rotate first block')
    first_element = block[0][1:9]
    first_element.append(block[0][0])
    block[0] = first_element

main_run()


